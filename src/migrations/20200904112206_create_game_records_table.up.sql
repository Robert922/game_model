CREATE TABLE `game_records` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '編號',
  `player_id` bigint unsigned NOT NULL COMMENT '玩家編號',
  `game_id` bigint unsigned NOT NULL COMMENT '遊戲編號',
  `table_id` bigint unsigned NOT NULL COMMENT '遊戲編號',
  `stake` decimal(12, 2) NOT NULL COMMENT '下注',
  `winnings` decimal(12, 2) NOT NULL COMMENT '贏錢',
  `start_source` decimal(12, 2) NOT NULL COMMENT '起始分數',
  `end_source` decimal(12, 2) NOT NULL COMMENT '結算分數',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '建立時間戳記',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新時間戳記',

  PRIMARY KEY (`id`),
  KEY `FK_user_game_records_player_id` (`player_id`),
  CONSTRAINT `FK_game_records_player_id` FOREIGN KEY (`player_id`) REFERENCES `players` (`id`) ON DELETE CASCADE,
  KEY `FK_user_game_records_game_id` (`game_id`),
  CONSTRAINT `FK_game_records_game_id` FOREIGN KEY (`game_id`) REFERENCES `games` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;