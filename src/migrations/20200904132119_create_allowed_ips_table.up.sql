CREATE TABLE `allowed_ips` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '編號',
  `agent_id` bigint(20) unsigned NOT NULL COMMENT '代理商編號',
  `ip` varchar(45) NOT NULL COMMENT 'IP',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '建立時間戳記',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新時間戳記',

  PRIMARY KEY (`id`),
  UNIQUE KEY `UNI_allowed_ips_agent_id_and_ip` (`agent_id`, `ip`),
  KEY `FK_allowed_ips_agent_id` (`agent_id`),
  CONSTRAINT `FK_allowed_ips_agent_id` FOREIGN KEY (`agent_id`) REFERENCES `agents` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;