CREATE TABLE `game_record_details` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '編號',
  `game_record_id` bigint unsigned NOT NULL COMMENT '遊戲紀錄編號',
  `stake` decimal(12, 2) NOT NULL COMMENT '下注',
  `winnings` decimal(12, 2) NOT NULL COMMENT '贏錢',
  `scatter` tinyint(1) unsigned NOT NULL COMMENT '特殊獎項數量',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '建立時間戳記',

  PRIMARY KEY (`id`),
  KEY `FK_game_record_details_game_record_id` (`game_record_id`),
  CONSTRAINT `FK_game_record_details_game_record_id` FOREIGN KEY (`game_record_id`) REFERENCES `game_records` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;