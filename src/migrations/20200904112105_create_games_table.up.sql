CREATE TABLE `games` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '編號',
  `name` varchar(255) NOT NULL COMMENT '名稱',
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否啟用',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '建立時間戳記',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新時間戳記',

  PRIMARY KEY (`id`),
  UNIQUE KEY `UNI_games_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;