CREATE TABLE `agents` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '編號',
  `country` tinyint(1) unsigned NOT NULL COMMENT '國家, 1: 馬來西亞, 2: 泰國, 3: 柬埔寨, 4: 緬甸, 5: 中國, 6: 越南, 7: 印尼',
  `name` varchar(60) NOT NULL COMMENT '名稱',
  `source` decimal(12, 2) NOT NULL COMMENT '分數',
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否啟用',
  `is_ip_filter_enabled` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否啟用 IP 允許名單',
  `auth_code` varchar(40) NOT NULL COMMENT '識別碼',
  `parent_id` bigint unsigned NOT NULL COMMENT '父層編號',
  `parent_tree` text COMMENT '父層排序樹',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '建立時間戳記',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新時間戳記',

  PRIMARY KEY (`id`),
  UNIQUE KEY `UNI_agents_full_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
