CREATE TABLE `user_group` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '編號',
  `user_id` bigint(20) unsigned NOT NULL COMMENT '使用者編號',
  `group_id` bigint(20) unsigned NOT NULL COMMENT '群組編號',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '建立時間戳記',

  PRIMARY KEY (`id`),
  KEY `FK_user_group_group_id` (`group_id`),
  CONSTRAINT `FK_user_group_group_id` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE,
  KEY `FK_user_group_user_id` (`user_id`),
  CONSTRAINT `FK_user_group_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;