CREATE TABLE `group_role` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '編號',
  `group_id` bigint(20) unsigned NOT NULL COMMENT '群組編號',
  `role_id` bigint(20) unsigned NOT NULL COMMENT '角色編號',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '建立時間戳記',

  PRIMARY KEY (`id`),
  KEY `FK_group_role_group_id` (`group_id`),
  CONSTRAINT `FK_group_role_group_id` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE,
  KEY `FK_group_role_role_id` (`role_id`),
  CONSTRAINT `FK_group_role_role_id` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;