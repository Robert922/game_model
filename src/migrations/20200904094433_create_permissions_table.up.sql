CREATE TABLE `permissions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '編號',
  `name` varchar(255) NOT NULL COMMENT '名稱',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '備註',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '建立時間戳記',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新時間戳記',

  PRIMARY KEY (`id`),
  UNIQUE KEY `UNI_permissions_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;