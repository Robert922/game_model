CREATE TABLE `operation_logs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '編號',
  `type` tinyint(1) unsigned NOT NULL COMMENT '類型: 1:緊急鎖定,2:修改密碼,3:重置二步驟驗證碼',
  `operator_id` bigint(20) unsigned NOT NULL COMMENT '操作者編號',
  `agent_id` bigint(20) unsigned NULL COMMENT '代理商編號',
  `player_id` bigint(20) unsigned NULL COMMENT '玩家編號',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '操作描述',
  `ip` varchar(45) NOT NULL COMMENT 'IP',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '建立時間戳記',

  PRIMARY KEY (`id`),
  KEY `FK_operation_logs_operator_id` (`operator_id`),
  CONSTRAINT `FK_operation_logs_operator_id` FOREIGN KEY (`operator_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  KEY `FK_operation_logs_player_id` (`player_id`),
  CONSTRAINT `FK_operation_logs_player_id` FOREIGN KEY (`player_id`) REFERENCES `players` (`id`) ON DELETE CASCADE,
  KEY `FK_operation_logs_agent_id` (`agent_id`),
  CONSTRAINT `FK_operation_logs_agent_id` FOREIGN KEY (`agent_id`) REFERENCES `agents` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;