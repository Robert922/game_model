CREATE TABLE `wallet_logs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '編號',
  `operator_id` bigint(20) unsigned NOT NULL COMMENT '操作者編號',
  `agent_id` bigint(20) unsigned NULL COMMENT '代理商編號',
  `player_id` bigint(20) unsigned NULL COMMENT '玩家編號',
  `source` decimal(12,2) NOT NULL COMMENT '分數',
  `start_source` decimal(12, 2) NOT NULL COMMENT '起始分數',
  `end_source` decimal(12, 2) NOT NULL COMMENT '結算分數',
  `ip` varchar(45) NOT NULL COMMENT 'IP',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '建立時間戳記',

  PRIMARY KEY (`id`),
  KEY `FK_wallet_logs_operator_id` (`operator_id`),
  CONSTRAINT `FK_wallet_logs_operator_id` FOREIGN KEY (`operator_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  KEY `FK_wallet_logs_player_id` (`player_id`),
  CONSTRAINT `FK_wallet_logs_player_id` FOREIGN KEY (`player_id`) REFERENCES `players` (`id`) ON DELETE CASCADE,
  KEY `FK_wallet_logs_agent_id` (`agent_id`),
  CONSTRAINT `FK_wallet_logs_agent_id` FOREIGN KEY (`agent_id`) REFERENCES `agents` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;