CREATE TABLE `players` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '編號',
  `agent_id` bigint(20) unsigned NOT NULL COMMENT '代理商編號',
  `source` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '分數',
  `full_name` varchar(60) NOT NULL COMMENT '帳號',
  `password` varchar(60) NOT NULL COMMENT '密碼',
  `username` varchar(60) NOT NULL COMMENT '暱稱',
  `phone` varchar(10) NOT NULL COMMENT '聯絡方式',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '備註',
  `otp_encrypt_key` varchar(255) NOT NULL DEFAULT '' COMMENT '二次驗證金鑰',
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否啟用',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '建立時間戳記',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新時間戳記',
  `last_login_at` bigint unsigned NULL DEFAULT NULL COMMENT '最後登入時間戳記',

  PRIMARY KEY (`id`),
  UNIQUE KEY `UNI_players_full_name` (`full_name`),
  UNIQUE KEY `UNI_players_username` (`username`),
  KEY `FK_players_agent_id` (`agent_id`),
  CONSTRAINT `FK_players_agent_id` FOREIGN KEY (`agent_id`) REFERENCES `agents` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;