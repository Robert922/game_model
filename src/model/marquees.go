package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
	"github.com/satori/go.uuid"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
	_ = uuid.UUID{}
)

// Marquees struct is a row record of the marquees table in the background database
type Marquees struct {
	//[ 0] id                                             ubigint              null: false  primary: true   isArray: false  auto: true   col: ubigint         len: -1      default: []
	ID uint64 `gorm:"primary_key;AUTO_INCREMENT;column:id;type:ubigint;" json:"id"` // 編號
	//[ 1] message                                        text(65535)          null: true   primary: false  isArray: false  auto: false  col: text            len: 65535   default: []
	Message string `gorm:"column:message;type:text;size:65535;" json:"message"` // 訊息
	//[ 2] is_enabled                                     tinyint              null: false  primary: false  isArray: false  auto: false  col: tinyint         len: -1      default: [1]
	IsEnabled int32 `gorm:"column:is_enabled;type:tinyint;default:1;" json:"is_enabled"` // 是否啟用
	//[ 3] created_at                                     timestamp            null: false  primary: false  isArray: false  auto: false  col: timestamp       len: -1      default: [CURRENT_TIMESTAMP]
	CreatedAt time.Time `gorm:"column:created_at;type:timestamp;default:CURRENT_TIMESTAMP;" json:"created_at"` // 建立時間戳記
	//[ 4] updated_at                                     timestamp            null: false  primary: false  isArray: false  auto: false  col: timestamp       len: -1      default: [CURRENT_TIMESTAMP]
	UpdatedAt time.Time `gorm:"column:updated_at;type:timestamp;default:CURRENT_TIMESTAMP;" json:"updated_at"` // 更新時間戳記

}

// TableName sets the insert table name for this struct type
func (m *Marquees) TableName() string {
	return "marquees"
}

// BeforeSave invoked before saving, return an error if field is not populated.
func (m *Marquees) BeforeSave() error {
	return nil
}

// Prepare invoked before saving, can be used to populate fields etc.
func (m *Marquees) Prepare() {
}

// Validate invoked before performing action, return an error if field is not populated.
func (m *Marquees) Validate(action Action) error {
	return nil
}
