package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
	"github.com/satori/go.uuid"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
	_ = uuid.UUID{}
)

// OperationLogs struct is a row record of the operation_logs table in the background database
type OperationLogs struct {
	//[ 0] id                                             ubigint              null: false  primary: true   isArray: false  auto: true   col: ubigint         len: -1      default: []
	ID uint64 `gorm:"primary_key;AUTO_INCREMENT;column:id;type:ubigint;" json:"id"` // 編號
	//[ 1] type                                           utinyint             null: false  primary: false  isArray: false  auto: false  col: utinyint        len: -1      default: []
	Type uint32 `gorm:"column:type;type:utinyint;" json:"type"` // 類型: 1:緊急鎖定,2:修改密碼,3:重置二步驟驗證碼
	//[ 2] operator_id                                    ubigint              null: false  primary: false  isArray: false  auto: false  col: ubigint         len: -1      default: []
	OperatorID uint64 `gorm:"column:operator_id;type:ubigint;" json:"operator_id"` // 操作者編號
	//[ 3] agent_id                                       ubigint              null: true   primary: false  isArray: false  auto: false  col: ubigint         len: -1      default: []
	AgentID sql.NullInt64 `gorm:"column:agent_id;type:ubigint;" json:"agent_id"` // 代理商編號
	//[ 4] player_id                                      ubigint              null: true   primary: false  isArray: false  auto: false  col: ubigint         len: -1      default: []
	PlayerID sql.NullInt64 `gorm:"column:player_id;type:ubigint;" json:"player_id"` // 玩家編號
	//[ 5] description                                    varchar(255)         null: false  primary: false  isArray: false  auto: false  col: varchar         len: 255     default: [""]
	Description string `gorm:"column:description;type:varchar;size:255;default:'';" json:"description"` // 操作描述
	//[ 6] ip                                             varchar(45)          null: false  primary: false  isArray: false  auto: false  col: varchar         len: 45      default: []
	IP string `gorm:"column:ip;type:varchar;size:45;" json:"ip"` // IP
	//[ 7] created_at                                     timestamp            null: false  primary: false  isArray: false  auto: false  col: timestamp       len: -1      default: [CURRENT_TIMESTAMP]
	CreatedAt time.Time `gorm:"column:created_at;type:timestamp;default:CURRENT_TIMESTAMP;" json:"created_at"` // 建立時間戳記

}

// TableName sets the insert table name for this struct type
func (o *OperationLogs) TableName() string {
	return "operation_logs"
}

// BeforeSave invoked before saving, return an error if field is not populated.
func (o *OperationLogs) BeforeSave() error {
	return nil
}

// Prepare invoked before saving, can be used to populate fields etc.
func (o *OperationLogs) Prepare() {
}

// Validate invoked before performing action, return an error if field is not populated.
func (o *OperationLogs) Validate(action Action) error {
	return nil
}
