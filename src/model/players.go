package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
	"github.com/satori/go.uuid"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
	_ = uuid.UUID{}
)

// Players struct is a row record of the players table in the background database
type Players struct {
	//[ 0] id                                             ubigint              null: false  primary: true   isArray: false  auto: true   col: ubigint         len: -1      default: []
	ID uint64 `gorm:"primary_key;AUTO_INCREMENT;column:id;type:ubigint;" json:"id"` // 編號
	//[ 1] agent_id                                       ubigint              null: false  primary: false  isArray: false  auto: false  col: ubigint         len: -1      default: []
	AgentID uint64 `gorm:"column:agent_id;type:ubigint;" json:"agent_id"` // 代理商編號
	//[ 2] source                                         decimal              null: false  primary: false  isArray: false  auto: false  col: decimal         len: -1      default: [0.00]
	Source float64 `gorm:"column:source;type:decimal;default:0.00;" json:"source"` // 分數
	//[ 3] full_name                                      varchar(60)          null: false  primary: false  isArray: false  auto: false  col: varchar         len: 60      default: []
	FullName string `gorm:"column:full_name;type:varchar;size:60;" json:"full_name"` // 帳號
	//[ 4] password                                       varchar(60)          null: false  primary: false  isArray: false  auto: false  col: varchar         len: 60      default: []
	Password string `gorm:"column:password;type:varchar;size:60;" json:"password"` // 密碼
	//[ 5] username                                       varchar(60)          null: false  primary: false  isArray: false  auto: false  col: varchar         len: 60      default: []
	Username string `gorm:"column:username;type:varchar;size:60;" json:"username"` // 暱稱
	//[ 6] phone                                          varchar(10)          null: false  primary: false  isArray: false  auto: false  col: varchar         len: 10      default: []
	Phone string `gorm:"column:phone;type:varchar;size:10;" json:"phone"` // 聯絡方式
	//[ 7] description                                    varchar(255)         null: false  primary: false  isArray: false  auto: false  col: varchar         len: 255     default: [""]
	Description string `gorm:"column:description;type:varchar;size:255;default:'';" json:"description"` // 備註
	//[ 8] otp_encrypt_key                                varchar(255)         null: false  primary: false  isArray: false  auto: false  col: varchar         len: 255     default: [""]
	OtpEncryptKey string `gorm:"column:otp_encrypt_key;type:varchar;size:255;default:'';" json:"otp_encrypt_key"` // 二次驗證金鑰
	//[ 9] is_enabled                                     tinyint              null: false  primary: false  isArray: false  auto: false  col: tinyint         len: -1      default: [1]
	IsEnabled int32 `gorm:"column:is_enabled;type:tinyint;default:1;" json:"is_enabled"` // 是否啟用
	//[10] created_at                                     timestamp            null: false  primary: false  isArray: false  auto: false  col: timestamp       len: -1      default: [CURRENT_TIMESTAMP]
	CreatedAt time.Time `gorm:"column:created_at;type:timestamp;default:CURRENT_TIMESTAMP;" json:"created_at"` // 建立時間戳記
	//[11] updated_at                                     timestamp            null: false  primary: false  isArray: false  auto: false  col: timestamp       len: -1      default: [CURRENT_TIMESTAMP]
	UpdatedAt time.Time `gorm:"column:updated_at;type:timestamp;default:CURRENT_TIMESTAMP;" json:"updated_at"` // 更新時間戳記
	//[12] last_login_at                                  ubigint              null: true   primary: false  isArray: false  auto: false  col: ubigint         len: -1      default: []
	LastLoginAt sql.NullInt64 `gorm:"column:last_login_at;type:ubigint;" json:"last_login_at"` // 最後登入時間戳記

}

// TableName sets the insert table name for this struct type
func (p *Players) TableName() string {
	return "players"
}

// BeforeSave invoked before saving, return an error if field is not populated.
func (p *Players) BeforeSave() error {
	return nil
}

// Prepare invoked before saving, can be used to populate fields etc.
func (p *Players) Prepare() {
}

// Validate invoked before performing action, return an error if field is not populated.
func (p *Players) Validate(action Action) error {
	return nil
}
