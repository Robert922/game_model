package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
	"github.com/satori/go.uuid"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
	_ = uuid.UUID{}
)

// GameRecords struct is a row record of the game_records table in the background database
type GameRecords struct {
	//[ 0] id                                             ubigint              null: false  primary: true   isArray: false  auto: true   col: ubigint         len: -1      default: []
	ID uint64 `gorm:"primary_key;AUTO_INCREMENT;column:id;type:ubigint;" json:"id"` // 編號
	//[ 1] player_id                                      ubigint              null: false  primary: false  isArray: false  auto: false  col: ubigint         len: -1      default: []
	PlayerID uint64 `gorm:"column:player_id;type:ubigint;" json:"player_id"` // 玩家編號
	//[ 2] game_id                                        ubigint              null: false  primary: false  isArray: false  auto: false  col: ubigint         len: -1      default: []
	GameID uint64 `gorm:"column:game_id;type:ubigint;" json:"game_id"` // 遊戲編號
	//[ 3] table_id                                       ubigint              null: false  primary: false  isArray: false  auto: false  col: ubigint         len: -1      default: []
	TableID uint64 `gorm:"column:table_id;type:ubigint;" json:"table_id"` // 遊戲編號
	//[ 4] stake                                          decimal              null: false  primary: false  isArray: false  auto: false  col: decimal         len: -1      default: []
	Stake float64 `gorm:"column:stake;type:decimal;" json:"stake"` // 下注
	//[ 5] winnings                                       decimal              null: false  primary: false  isArray: false  auto: false  col: decimal         len: -1      default: []
	Winnings float64 `gorm:"column:winnings;type:decimal;" json:"winnings"` // 贏錢
	//[ 6] start_source                                   decimal              null: false  primary: false  isArray: false  auto: false  col: decimal         len: -1      default: []
	StartSource float64 `gorm:"column:start_source;type:decimal;" json:"start_source"` // 起始分數
	//[ 7] end_source                                     decimal              null: false  primary: false  isArray: false  auto: false  col: decimal         len: -1      default: []
	EndSource float64 `gorm:"column:end_source;type:decimal;" json:"end_source"` // 結算分數
	//[ 8] created_at                                     timestamp            null: false  primary: false  isArray: false  auto: false  col: timestamp       len: -1      default: [CURRENT_TIMESTAMP]
	CreatedAt time.Time `gorm:"column:created_at;type:timestamp;default:CURRENT_TIMESTAMP;" json:"created_at"` // 建立時間戳記
	//[ 9] updated_at                                     timestamp            null: false  primary: false  isArray: false  auto: false  col: timestamp       len: -1      default: [CURRENT_TIMESTAMP]
	UpdatedAt time.Time `gorm:"column:updated_at;type:timestamp;default:CURRENT_TIMESTAMP;" json:"updated_at"` // 更新時間戳記

}

// TableName sets the insert table name for this struct type
func (g *GameRecords) TableName() string {
	return "game_records"
}

// BeforeSave invoked before saving, return an error if field is not populated.
func (g *GameRecords) BeforeSave() error {
	return nil
}

// Prepare invoked before saving, can be used to populate fields etc.
func (g *GameRecords) Prepare() {
}

// Validate invoked before performing action, return an error if field is not populated.
func (g *GameRecords) Validate(action Action) error {
	return nil
}
