package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
	"github.com/satori/go.uuid"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
	_ = uuid.UUID{}
)

// AllowedIps struct is a row record of the allowed_ips table in the background database
type AllowedIps struct {
	//[ 0] id                                             ubigint              null: false  primary: true   isArray: false  auto: true   col: ubigint         len: -1      default: []
	ID uint64 `gorm:"primary_key;AUTO_INCREMENT;column:id;type:ubigint;" json:"id"` // 編號
	//[ 1] agent_id                                       ubigint              null: false  primary: false  isArray: false  auto: false  col: ubigint         len: -1      default: []
	AgentID uint64 `gorm:"column:agent_id;type:ubigint;" json:"agent_id"` // 代理商編號
	//[ 2] ip                                             varchar(45)          null: false  primary: false  isArray: false  auto: false  col: varchar         len: 45      default: []
	IP string `gorm:"column:ip;type:varchar;size:45;" json:"ip"` // IP
	//[ 3] created_at                                     timestamp            null: false  primary: false  isArray: false  auto: false  col: timestamp       len: -1      default: [CURRENT_TIMESTAMP]
	CreatedAt time.Time `gorm:"column:created_at;type:timestamp;default:CURRENT_TIMESTAMP;" json:"created_at"` // 建立時間戳記
	//[ 4] updated_at                                     timestamp            null: false  primary: false  isArray: false  auto: false  col: timestamp       len: -1      default: [CURRENT_TIMESTAMP]
	UpdatedAt time.Time `gorm:"column:updated_at;type:timestamp;default:CURRENT_TIMESTAMP;" json:"updated_at"` // 更新時間戳記

}

// TableName sets the insert table name for this struct type
func (a *AllowedIps) TableName() string {
	return "allowed_ips"
}

// BeforeSave invoked before saving, return an error if field is not populated.
func (a *AllowedIps) BeforeSave() error {
	return nil
}

// Prepare invoked before saving, can be used to populate fields etc.
func (a *AllowedIps) Prepare() {
}

// Validate invoked before performing action, return an error if field is not populated.
func (a *AllowedIps) Validate(action Action) error {
	return nil
}
