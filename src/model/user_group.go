package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
	"github.com/satori/go.uuid"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
	_ = uuid.UUID{}
)

// UserGroup struct is a row record of the user_group table in the background database
type UserGroup struct {
	//[ 0] id                                             ubigint              null: false  primary: true   isArray: false  auto: true   col: ubigint         len: -1      default: []
	ID uint64 `gorm:"primary_key;AUTO_INCREMENT;column:id;type:ubigint;" json:"id"` // 編號
	//[ 1] user_id                                        ubigint              null: false  primary: false  isArray: false  auto: false  col: ubigint         len: -1      default: []
	UserID uint64 `gorm:"column:user_id;type:ubigint;" json:"user_id"` // 使用者編號
	//[ 2] group_id                                       ubigint              null: false  primary: false  isArray: false  auto: false  col: ubigint         len: -1      default: []
	GroupID uint64 `gorm:"column:group_id;type:ubigint;" json:"group_id"` // 群組編號
	//[ 3] created_at                                     timestamp            null: false  primary: false  isArray: false  auto: false  col: timestamp       len: -1      default: [CURRENT_TIMESTAMP]
	CreatedAt time.Time `gorm:"column:created_at;type:timestamp;default:CURRENT_TIMESTAMP;" json:"created_at"` // 建立時間戳記

}

// TableName sets the insert table name for this struct type
func (u *UserGroup) TableName() string {
	return "user_group"
}

// BeforeSave invoked before saving, return an error if field is not populated.
func (u *UserGroup) BeforeSave() error {
	return nil
}

// Prepare invoked before saving, can be used to populate fields etc.
func (u *UserGroup) Prepare() {
}

// Validate invoked before performing action, return an error if field is not populated.
func (u *UserGroup) Validate(action Action) error {
	return nil
}
