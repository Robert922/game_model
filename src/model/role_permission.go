package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
	"github.com/satori/go.uuid"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
	_ = uuid.UUID{}
)

// RolePermission struct is a row record of the role_permission table in the background database
type RolePermission struct {
	//[ 0] id                                             ubigint              null: false  primary: true   isArray: false  auto: true   col: ubigint         len: -1      default: []
	ID uint64 `gorm:"primary_key;AUTO_INCREMENT;column:id;type:ubigint;" json:"id"` // 編號
	//[ 1] role_id                                        ubigint              null: false  primary: false  isArray: false  auto: false  col: ubigint         len: -1      default: []
	RoleID uint64 `gorm:"column:role_id;type:ubigint;" json:"role_id"` // 角色編號
	//[ 2] permission_id                                  ubigint              null: false  primary: false  isArray: false  auto: false  col: ubigint         len: -1      default: []
	PermissionID uint64 `gorm:"column:permission_id;type:ubigint;" json:"permission_id"` // 權限編號
	//[ 3] created_at                                     timestamp            null: false  primary: false  isArray: false  auto: false  col: timestamp       len: -1      default: [CURRENT_TIMESTAMP]
	CreatedAt time.Time `gorm:"column:created_at;type:timestamp;default:CURRENT_TIMESTAMP;" json:"created_at"` // 建立時間戳記

}

// TableName sets the insert table name for this struct type
func (r *RolePermission) TableName() string {
	return "role_permission"
}

// BeforeSave invoked before saving, return an error if field is not populated.
func (r *RolePermission) BeforeSave() error {
	return nil
}

// Prepare invoked before saving, can be used to populate fields etc.
func (r *RolePermission) Prepare() {
}

// Validate invoked before performing action, return an error if field is not populated.
func (r *RolePermission) Validate(action Action) error {
	return nil
}
