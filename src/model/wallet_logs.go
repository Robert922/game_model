package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
	"github.com/satori/go.uuid"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
	_ = uuid.UUID{}
)

// WalletLogs struct is a row record of the wallet_logs table in the background database
type WalletLogs struct {
	//[ 0] id                                             ubigint              null: false  primary: true   isArray: false  auto: true   col: ubigint         len: -1      default: []
	ID uint64 `gorm:"primary_key;AUTO_INCREMENT;column:id;type:ubigint;" json:"id"` // 編號
	//[ 1] operator_id                                    ubigint              null: false  primary: false  isArray: false  auto: false  col: ubigint         len: -1      default: []
	OperatorID uint64 `gorm:"column:operator_id;type:ubigint;" json:"operator_id"` // 操作者編號
	//[ 2] agent_id                                       ubigint              null: true   primary: false  isArray: false  auto: false  col: ubigint         len: -1      default: []
	AgentID sql.NullInt64 `gorm:"column:agent_id;type:ubigint;" json:"agent_id"` // 代理商編號
	//[ 3] player_id                                      ubigint              null: true   primary: false  isArray: false  auto: false  col: ubigint         len: -1      default: []
	PlayerID sql.NullInt64 `gorm:"column:player_id;type:ubigint;" json:"player_id"` // 玩家編號
	//[ 4] source                                         decimal              null: false  primary: false  isArray: false  auto: false  col: decimal         len: -1      default: []
	Source float64 `gorm:"column:source;type:decimal;" json:"source"` // 分數
	//[ 5] start_source                                   decimal              null: false  primary: false  isArray: false  auto: false  col: decimal         len: -1      default: []
	StartSource float64 `gorm:"column:start_source;type:decimal;" json:"start_source"` // 起始分數
	//[ 6] end_source                                     decimal              null: false  primary: false  isArray: false  auto: false  col: decimal         len: -1      default: []
	EndSource float64 `gorm:"column:end_source;type:decimal;" json:"end_source"` // 結算分數
	//[ 7] ip                                             varchar(45)          null: false  primary: false  isArray: false  auto: false  col: varchar         len: 45      default: []
	IP string `gorm:"column:ip;type:varchar;size:45;" json:"ip"` // IP
	//[ 8] created_at                                     timestamp            null: false  primary: false  isArray: false  auto: false  col: timestamp       len: -1      default: [CURRENT_TIMESTAMP]
	CreatedAt time.Time `gorm:"column:created_at;type:timestamp;default:CURRENT_TIMESTAMP;" json:"created_at"` // 建立時間戳記

}

// TableName sets the insert table name for this struct type
func (w *WalletLogs) TableName() string {
	return "wallet_logs"
}

// BeforeSave invoked before saving, return an error if field is not populated.
func (w *WalletLogs) BeforeSave() error {
	return nil
}

// Prepare invoked before saving, can be used to populate fields etc.
func (w *WalletLogs) Prepare() {
}

// Validate invoked before performing action, return an error if field is not populated.
func (w *WalletLogs) Validate(action Action) error {
	return nil
}
