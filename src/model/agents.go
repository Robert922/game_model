package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
	"github.com/satori/go.uuid"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
	_ = uuid.UUID{}
)

// Agents struct is a row record of the agents table in the background database
type Agents struct {
	//[ 0] id                                             ubigint              null: false  primary: true   isArray: false  auto: true   col: ubigint         len: -1      default: []
	ID uint64 `gorm:"primary_key;AUTO_INCREMENT;column:id;type:ubigint;" json:"id"` // 編號
	//[ 1] country                                        utinyint             null: false  primary: false  isArray: false  auto: false  col: utinyint        len: -1      default: []
	Country uint32 `gorm:"column:country;type:utinyint;" json:"country"` // 國家, 1: 馬來西亞, 2: 泰國, 3: 柬埔寨, 4: 緬甸, 5: 中國, 6: 越南, 7: 印尼
	//[ 2] name                                           varchar(60)          null: false  primary: false  isArray: false  auto: false  col: varchar         len: 60      default: []
	Name string `gorm:"column:name;type:varchar;size:60;" json:"name"` // 名稱
	//[ 3] source                                         decimal              null: false  primary: false  isArray: false  auto: false  col: decimal         len: -1      default: []
	Source float64 `gorm:"column:source;type:decimal;" json:"source"` // 分數
	//[ 4] is_enabled                                     tinyint              null: false  primary: false  isArray: false  auto: false  col: tinyint         len: -1      default: [1]
	IsEnabled int32 `gorm:"column:is_enabled;type:tinyint;default:1;" json:"is_enabled"` // 是否啟用
	//[ 5] is_ip_filter_enabled                           tinyint              null: false  primary: false  isArray: false  auto: false  col: tinyint         len: -1      default: [0]
	IsIPFilterEnabled int32 `gorm:"column:is_ip_filter_enabled;type:tinyint;default:0;" json:"is_ip_filter_enabled"` // 是否啟用 IP 允許名單
	//[ 6] auth_code                                      varchar(40)          null: false  primary: false  isArray: false  auto: false  col: varchar         len: 40      default: []
	AuthCode string `gorm:"column:auth_code;type:varchar;size:40;" json:"auth_code"` // 識別碼
	//[ 7] parent_id                                      ubigint              null: false  primary: false  isArray: false  auto: false  col: ubigint         len: -1      default: []
	ParentID uint64 `gorm:"column:parent_id;type:ubigint;" json:"parent_id"` // 父層編號
	//[ 8] parent_tree                                    text(65535)          null: true   primary: false  isArray: false  auto: false  col: text            len: 65535   default: []
	ParentTree string `gorm:"column:parent_tree;type:text;size:65535;" json:"parent_tree"` // 父層排序樹
	//[ 9] created_at                                     timestamp            null: false  primary: false  isArray: false  auto: false  col: timestamp       len: -1      default: [CURRENT_TIMESTAMP]
	CreatedAt time.Time `gorm:"column:created_at;type:timestamp;default:CURRENT_TIMESTAMP;" json:"created_at"` // 建立時間戳記
	//[10] updated_at                                     timestamp            null: false  primary: false  isArray: false  auto: false  col: timestamp       len: -1      default: [CURRENT_TIMESTAMP]
	UpdatedAt time.Time `gorm:"column:updated_at;type:timestamp;default:CURRENT_TIMESTAMP;" json:"updated_at"` // 更新時間戳記

}

// TableName sets the insert table name for this struct type
func (a *Agents) TableName() string {
	return "agents"
}

// BeforeSave invoked before saving, return an error if field is not populated.
func (a *Agents) BeforeSave() error {
	return nil
}

// Prepare invoked before saving, can be used to populate fields etc.
func (a *Agents) Prepare() {
}

// Validate invoked before performing action, return an error if field is not populated.
func (a *Agents) Validate(action Action) error {
	return nil
}
