package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
	"github.com/satori/go.uuid"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
	_ = uuid.UUID{}
)

// Permissions struct is a row record of the permissions table in the background database
type Permissions struct {
	//[ 0] id                                             ubigint              null: false  primary: true   isArray: false  auto: true   col: ubigint         len: -1      default: []
	ID uint64 `gorm:"primary_key;AUTO_INCREMENT;column:id;type:ubigint;" json:"id"` // 編號
	//[ 1] name                                           varchar(255)         null: false  primary: false  isArray: false  auto: false  col: varchar         len: 255     default: []
	Name string `gorm:"column:name;type:varchar;size:255;" json:"name"` // 名稱
	//[ 2] description                                    varchar(255)         null: false  primary: false  isArray: false  auto: false  col: varchar         len: 255     default: [""]
	Description string `gorm:"column:description;type:varchar;size:255;default:'';" json:"description"` // 備註
	//[ 3] created_at                                     timestamp            null: false  primary: false  isArray: false  auto: false  col: timestamp       len: -1      default: [CURRENT_TIMESTAMP]
	CreatedAt time.Time `gorm:"column:created_at;type:timestamp;default:CURRENT_TIMESTAMP;" json:"created_at"` // 建立時間戳記
	//[ 4] updated_at                                     timestamp            null: false  primary: false  isArray: false  auto: false  col: timestamp       len: -1      default: [CURRENT_TIMESTAMP]
	UpdatedAt time.Time `gorm:"column:updated_at;type:timestamp;default:CURRENT_TIMESTAMP;" json:"updated_at"` // 更新時間戳記

}

// TableName sets the insert table name for this struct type
func (p *Permissions) TableName() string {
	return "permissions"
}

// BeforeSave invoked before saving, return an error if field is not populated.
func (p *Permissions) BeforeSave() error {
	return nil
}

// Prepare invoked before saving, can be used to populate fields etc.
func (p *Permissions) Prepare() {
}

// Validate invoked before performing action, return an error if field is not populated.
func (p *Permissions) Validate(action Action) error {
	return nil
}
