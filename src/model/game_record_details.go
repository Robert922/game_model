package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
	"github.com/satori/go.uuid"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
	_ = uuid.UUID{}
)

// GameRecordDetails struct is a row record of the game_record_details table in the background database
type GameRecordDetails struct {
	//[ 0] id                                             ubigint              null: false  primary: true   isArray: false  auto: true   col: ubigint         len: -1      default: []
	ID uint64 `gorm:"primary_key;AUTO_INCREMENT;column:id;type:ubigint;" json:"id"` // 編號
	//[ 1] game_record_id                                 ubigint              null: false  primary: false  isArray: false  auto: false  col: ubigint         len: -1      default: []
	GameRecordID uint64 `gorm:"column:game_record_id;type:ubigint;" json:"game_record_id"` // 遊戲紀錄編號
	//[ 2] stake                                          decimal              null: false  primary: false  isArray: false  auto: false  col: decimal         len: -1      default: []
	Stake float64 `gorm:"column:stake;type:decimal;" json:"stake"` // 下注
	//[ 3] winnings                                       decimal              null: false  primary: false  isArray: false  auto: false  col: decimal         len: -1      default: []
	Winnings float64 `gorm:"column:winnings;type:decimal;" json:"winnings"` // 贏錢
	//[ 4] scatter                                        utinyint             null: false  primary: false  isArray: false  auto: false  col: utinyint        len: -1      default: []
	Scatter uint32 `gorm:"column:scatter;type:utinyint;" json:"scatter"` // 特殊獎項數量
	//[ 5] created_at                                     timestamp            null: false  primary: false  isArray: false  auto: false  col: timestamp       len: -1      default: [CURRENT_TIMESTAMP]
	CreatedAt time.Time `gorm:"column:created_at;type:timestamp;default:CURRENT_TIMESTAMP;" json:"created_at"` // 建立時間戳記

}

// TableName sets the insert table name for this struct type
func (g *GameRecordDetails) TableName() string {
	return "game_record_details"
}

// BeforeSave invoked before saving, return an error if field is not populated.
func (g *GameRecordDetails) BeforeSave() error {
	return nil
}

// Prepare invoked before saving, can be used to populate fields etc.
func (g *GameRecordDetails) Prepare() {
}

// Validate invoked before performing action, return an error if field is not populated.
func (g *GameRecordDetails) Validate(action Action) error {
	return nil
}
