module bitbucket.org/Robert922/game_model

go 1.15

require (
	bitbucket.org/peter1003/common v0.0.0-20200910074614-20da90747c83
	github.com/go-sql-driver/mysql v1.5.0 // indirect
	github.com/guregu/null v4.0.0+incompatible
	github.com/jinzhu/gorm v1.9.16 // indirect
	github.com/satori/go.uuid v1.2.0
	github.com/urfave/cli/v2 v2.2.0
	golang.org/x/crypto v0.0.0-20200709230013-948cd5f35899
)
